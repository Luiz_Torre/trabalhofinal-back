class CreateDepartments < ActiveRecord::Migration[6.1]
  def change
    create_table :departments do |t|
      t.string :name
      t.string :knowledgearea
      t.string :HeadquartersCampus
      t.references :teacher, null: false, foreign_key: true
      t.references :department_coordinator, null: false, foreign_key: true

      t.timestamps
    end
  end
end
