# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_26_220446) do

  create_table "coursecoordinators", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "curses", force: :cascade do |t|
    t.integer "subject_id", null: false
    t.integer "numberofstudent"
    t.integer "coursecoordinator_id", null: false
    t.string "knowledgearea"
    t.string "HeadquartersCampus"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["coursecoordinator_id"], name: "index_curses_on_coursecoordinator_id"
    t.index ["subject_id"], name: "index_curses_on_subject_id"
  end

  create_table "departmentcoordinators", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string "name"
    t.string "knowledgearea"
    t.string "HeadquartersCampus"
    t.integer "teacher_id", null: false
    t.integer "department_coordinator_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["department_coordinator_id"], name: "index_departments_on_department_coordinator_id"
    t.index ["teacher_id"], name: "index_departments_on_teacher_id"
  end

  create_table "grades", force: :cascade do |t|
    t.integer "student_id", null: false
    t.integer "subject_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["student_id"], name: "index_grades_on_student_id"
    t.index ["subject_id"], name: "index_grades_on_subject_id"
  end

  create_table "prerequisites", force: :cascade do |t|
    t.integer "subject_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["subject_id"], name: "index_prerequisites_on_subject_id"
  end

  create_table "principals", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "school_years", force: :cascade do |t|
    t.string "status"
    t.string "year"
    t.string "half"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "schoolclasses", force: :cascade do |t|
    t.string "calendar"
    t.string "classroom"
    t.integer "numberofstudents"
    t.string "name"
    t.integer "teacher_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["teacher_id"], name: "index_schoolclasses_on_teacher_id"
  end

  create_table "students", force: :cascade do |t|
    t.integer "curse_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["curse_id"], name: "index_students_on_curse_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.string "knowledgearea"
    t.integer "workload"
    t.integer "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["student_id"], name: "index_subjects_on_student_id"
  end

  create_table "subjectsstudieds", force: :cascade do |t|
    t.integer "student_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["student_id"], name: "index_subjectsstudieds_on_student_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.integer "subject_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["subject_id"], name: "index_teachers_on_subject_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "nationality"
    t.string "state"
    t.string "RG"
    t.date "birthdate"
    t.string "email"
    t.integer "role"
    t.string "CPF"
    t.string "password", default: "123456"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "curses", "coursecoordinators"
  add_foreign_key "curses", "subjects"
  add_foreign_key "departments", "department_coordinators"
  add_foreign_key "departments", "teachers"
  add_foreign_key "grades", "students"
  add_foreign_key "grades", "subjects"
  add_foreign_key "prerequisites", "subjects"
  add_foreign_key "schoolclasses", "teachers"
  add_foreign_key "students", "curses"
  add_foreign_key "subjects", "students"
  add_foreign_key "subjectsstudieds", "students"
  add_foreign_key "teachers", "subjects"
end
