class SubjectsstudiedsController < ApplicationController
  before_action :set_subjectsstudied, only: [:show, :update, :destroy]

  # GET /subjectsstudieds
  def index
    @subjectsstudieds = Subjectsstudied.all

    render json: @subjectsstudieds
  end

  # GET /subjectsstudieds/1
  def show
    render json: @subjectsstudied
  end

  # POST /subjectsstudieds
  def create
    @subjectsstudied = Subjectsstudied.new(subjectsstudied_params)

    if @subjectsstudied.save
      render json: @subjectsstudied, status: :created, location: @subjectsstudied
    else
      render json: @subjectsstudied.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /subjectsstudieds/1
  def update
    if @subjectsstudied.update(subjectsstudied_params)
      render json: @subjectsstudied
    else
      render json: @subjectsstudied.errors, status: :unprocessable_entity
    end
  end

  # DELETE /subjectsstudieds/1
  def destroy
    @subjectsstudied.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subjectsstudied
      @subjectsstudied = Subjectsstudied.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def subjectsstudied_params
      params.require(:subjectsstudied).permit(:student_id)
    end
end
