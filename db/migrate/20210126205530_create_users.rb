class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :nationality
      t.string :state
      t.string :RG
      t.date :birthdate
      t.string :email
      t.integer :role
      t.string :CPF
      t.string :password , default: 123456

      t.timestamps
    end
  end
end
