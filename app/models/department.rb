class Department < ApplicationRecord
  belongs_to :teacher
  belongs_to :department_coordinator
end
