Rails.application.routes.draw do
  resources :curses
  resources :subjectsstudieds
  resources :prerequisites
  resources :grades
  resources :subjects
  resources :students
  resources :schoolclasses
  resources :departments
  resources :teachers
  resources :school_years
  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
