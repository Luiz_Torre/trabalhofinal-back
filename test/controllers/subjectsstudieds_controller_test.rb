require "test_helper"

class SubjectsstudiedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subjectsstudied = subjectsstudieds(:one)
  end

  test "should get index" do
    get subjectsstudieds_url, as: :json
    assert_response :success
  end

  test "should create subjectsstudied" do
    assert_difference('Subjectsstudied.count') do
      post subjectsstudieds_url, params: { subjectsstudied: { student_id: @subjectsstudied.student_id } }, as: :json
    end

    assert_response 201
  end

  test "should show subjectsstudied" do
    get subjectsstudied_url(@subjectsstudied), as: :json
    assert_response :success
  end

  test "should update subjectsstudied" do
    patch subjectsstudied_url(@subjectsstudied), params: { subjectsstudied: { student_id: @subjectsstudied.student_id } }, as: :json
    assert_response 200
  end

  test "should destroy subjectsstudied" do
    assert_difference('Subjectsstudied.count', -1) do
      delete subjectsstudied_url(@subjectsstudied), as: :json
    end

    assert_response 204
  end
end
