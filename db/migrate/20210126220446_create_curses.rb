class CreateCurses < ActiveRecord::Migration[6.1]
  def change
    create_table :curses do |t|
      t.references :subject, null: false, foreign_key: true
      t.integer :numberofstudent
      t.references :coursecoordinator, null: false, foreign_key: true
      t.string :knowledgearea
      t.string :HeadquartersCampus
      t.string :name

      t.timestamps
    end
  end
end
