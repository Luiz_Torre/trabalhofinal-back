require "test_helper"

class CursesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @curse = curses(:one)
  end

  test "should get index" do
    get curses_url, as: :json
    assert_response :success
  end

  test "should create curse" do
    assert_difference('Curse.count') do
      post curses_url, params: { curse: { HeadquartersCampus: @curse.HeadquartersCampus, coursecoordinator_id: @curse.coursecoordinator_id, knowledgearea: @curse.knowledgearea, name: @curse.name, numberofstudent: @curse.numberofstudent, subject_id: @curse.subject_id } }, as: :json
    end

    assert_response 201
  end

  test "should show curse" do
    get curse_url(@curse), as: :json
    assert_response :success
  end

  test "should update curse" do
    patch curse_url(@curse), params: { curse: { HeadquartersCampus: @curse.HeadquartersCampus, coursecoordinator_id: @curse.coursecoordinator_id, knowledgearea: @curse.knowledgearea, name: @curse.name, numberofstudent: @curse.numberofstudent, subject_id: @curse.subject_id } }, as: :json
    assert_response 200
  end

  test "should destroy curse" do
    assert_difference('Curse.count', -1) do
      delete curse_url(@curse), as: :json
    end

    assert_response 204
  end
end
